from asynctest import TestCase, main
from httpx import AsyncClient
from starlette import status
from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import JSONResponse
from starlette.routing import Route
from typesystem import Boolean, Schema, String

from uniform import Uniform
from uniform.base import _get_app_methods

uniform = Uniform(app="starlette")


class TestSchema(Schema):
    test = Boolean()


@uniform.validate(TestSchema)
async def home(request: Request, data: TestSchema) -> JSONResponse:
    return JSONResponse(content={"ok": True, "description": None, "result": dict(data)})


ROUTES = [
    Route("/", endpoint=home, methods=["POST"]),
]

APP = Starlette(routes=ROUTES)


class UniformTest(TestCase):
    async def test_allowed_app_types(self) -> None:
        with self.assertRaises(ValueError):
            Uniform(app="not starlette")

        with self.assertRaises(ValueError):
            _get_app_methods("not starlette")

        Uniform(app="starlette")

    async def test_empty_request(self) -> None:
        async with AsyncClient(app=APP, base_url="http://test") as client:
            response = await client.post("http://test/")

            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(
                response.json(),
                {
                    "ok": False,
                    "description": "Bad Request",
                    "result": {"content_type": "Unsupported Content-Type."},
                },
            )

    async def test_valid_form_request(self) -> None:
        async with AsyncClient(app=APP, base_url="http://test") as client:
            response = await client.post(
                "http://test/",
                data={"test": True},
            )

            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(
                response.json(),
                {
                    "ok": True,
                    "description": None,
                    "result": {"test": True},
                },
            )

    async def test_valid_json_request(self) -> None:
        async with AsyncClient(app=APP, base_url="http://test") as client:
            response = await client.post(
                "http://test/",
                json={"test": True},
            )

            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertEqual(
                response.json(),
                {
                    "ok": True,
                    "description": None,
                    "result": {"test": True},
                },
            )

    async def test_invalid_json_request(self) -> None:
        async with AsyncClient(app=APP, base_url="http://test") as client:
            response = await client.post(
                "http://test/",
                json={"test": "not boolean"},
            )

            self.assertEqual(response.status_code, status.HTTP_422_UNPROCESSABLE_ENTITY)
            self.assertEqual(
                response.json(),
                {
                    "ok": False,
                    "description": "Schema Validation Failed",
                    "result": {"test": "Must be a boolean."},
                },
            )


if __name__ == "__main__":
    main()
