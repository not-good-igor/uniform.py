from typing import Any, Awaitable, Callable, Dict, List, Tuple, Type, Union

from typesystem import Schema

_SUPPORTED_APPS: List[str] = ["starlette"]


def _get_app_methods(
    app: str,
) -> Tuple[
    Callable[..., Awaitable[Dict[str, Any]]],
    Union[Callable[[Dict[str, Any], int], Any], Type[Any]],
]:
    if app == "starlette":
        from ._starlette import JSONResponse, get_data  # type: ignore

        return get_data, JSONResponse

    raise ValueError(f"Unsupported app, choose from: {','.join(_SUPPORTED_APPS)}")


class Uniform:
    def __init__(self, app: str) -> None:
        if not app in _SUPPORTED_APPS:
            raise ValueError(
                f"Unsupported app, choose from: {','.join(_SUPPORTED_APPS)}"
            )

        self.get_data, self.json_response = _get_app_methods(app)

    def validate(self, schema: Type[Schema]) -> Callable:
        def outer_wrapper(endpoint: Callable) -> Callable:
            async def endpoint_wrapper(*args: Any, **kwargs: Any) -> Any:
                try:
                    data = await self.get_data(*args, **kwargs)
                except ValueError as error:
                    return self.json_response(
                        {
                            "ok": False,
                            "description": "Bad Request",
                            "result": {"content_type": str(error)},
                        },
                        400,
                    )

                processed_data, errors = schema.validate_or_error(data)
                if errors:
                    return self.json_response(
                        {
                            "ok": False,
                            "description": "Schema Validation Failed",
                            "result": dict(errors),
                        },
                        422,
                    )

                return await endpoint(*args, **kwargs, data=processed_data)

            return endpoint_wrapper

        return outer_wrapper
