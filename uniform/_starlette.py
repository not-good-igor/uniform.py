from typing import Any, Awaitable, Callable, Dict

try:
    from starlette.requests import Request
    from starlette.responses import JSONResponse
except ImportError:  # pragma: no cover
    raise ImportError("Package 'starlette' should be installed to use this module!")


async def _get_form(request: Request) -> Dict[str, Any]:
    return dict(await request.form())


async def _get_json(request: Request) -> Dict[str, Any]:
    return await request.json()


_DATA_GETTERS: Dict[str, Callable[[Request], Awaitable[Dict[str, Any]]]] = {
    "application/x-www-form-urlencoded": _get_form,
    "application/json": _get_json,
}


async def get_data(request: Request) -> Dict[str, Any]:
    content_type = request.headers.get("Content-Type")
    data_getter = _DATA_GETTERS.get(content_type, None)
    if not data_getter:
        raise ValueError("Unsupported Content-Type.")

    return await data_getter(request)
