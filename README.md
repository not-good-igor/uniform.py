<p align="center">
  <img width="240px" src="https://gitlab.com/not-good-igor/uniform.py/raw/master/docs/img/uniform.png" alt='uniform'>
</p>
<p align="center">
📋 <em>Uniform - dress your form processing endpoints.</em> 📋
</p>

---

<p align="center">
<a href="https://gitlab.com/not-good-igor/uniform.py/commits/master">
    <img src="https://gitlab.com/not-good-igor/uniform.py/badges/master/pipeline.svg" alt="Pipeline Status">
</a>
<a href="https://gitlab.com/not-good-igor/uniform.py/commits/master">
    <img src="https://gitlab.com/not-good-igor/uniform.py/badges/master/coverage.svg" alt="Coverage Report">
</a>
<a href="https://pypi.org/project/uniform">
    <img src="https://img.shields.io/pypi/v/uniform" alt="PyPI: uniform">
</a>
<a href="https://github.com/ambv/black">
    <img src="https://img.shields.io/badge/code_style-black-000000.svg" alt="Code Style: Black">
</a>
</p>

---

# Introduction

Uniform is a form processing tool that can be easily plugged into your Starlette backend.

It gives you the following:

* Extract and validate form data against `typesystem.Schema`
* Return 400 Bad Request with validation problems explained
* 100% test coverage.
* 100% type annotated codebase.

## Requirements

Python 3.6+

## Installation

```shell
$ pip3 install uniform
```

## Example

**example.py**:

```python
import uvicorn

from starlette.applications import Starlette
from starlette.requests import Request
from starlette.responses import PlainTextResponse
from typesystem import Schema, Boolean

from uniform import Uniform, validate

app = Starlette()


class TestSchema(Schema):
    test = Boolean()


@app.route("/")
@validate(Uniform(TestSchema))
async def home(request: Request) -> PlainTextResponse:
    return JSONResponse(
        content={"ok": True, "description": None, "result": dict(request.state.data)}
    )


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8080)
```

Then run the application...

```shell
$ python example.py
```

Send POST request to `http://127.0.0.1:8080/` with `application/json`
or `application/x-www-form-urlencoded` data and it will be validated.

## Dependencies

Uniform work with [`starlette`][starlette] endpoints and validate forms using [`typesystem`][typesystem] library.

If you want to parse `application/x-www-form-urlencoded` form data - you will also need a [`python-multipart`][python-multipart].

<p align="center">&mdash; 📋 &mdash;</p>
<p align="center"><i>Uniform is <a href="https://gitlab.com/not-good-igor/uniform.py/blob/master/LICENSE">Unlicensed</a> code in Public Domain.</i></p>

[starlette]: https://www.starlette.io/
[typesystem]: https://www.encode.io/typesystem/
[python-multipart]: https://andrew-d.github.io/python-multipart/